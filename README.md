
<!-- ![Image](https://th.bing.com/th/id/OIP.6nMDPk6lvz_mBpl-M3ktXwHaEK?pid=ImgDet&rs=1) 
 -->

```

                                        Console.log("Hey Buddy!"); 


```
___

# 🇳🇵 <strong> <i> Sagar Timalsena </i> </strong>

I'm Sagar from Nepal, and I do content on Design and Development. I really enjoy learning Programming languages and frameworks, as well as work in Develpoment and Operation (DevOps). Graphics Designer, Finding Bug in Website Reporting them is main interest.  

___

</br>
📌 <strong> <i> Github  </i> </strong>
</br>

![Sagar Github Stats](
https://github-readme-stats.vercel.app/api?username=Sagar1555&theme=radical)



___

### Languages and Tools


<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mysql/mysql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png"></code>

</br>


# Social Media 
<br>
<a href="https://twitter.com/@sagarr679">
  <img align="left" alt="Sagar's Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />
</a>
<a href="https://www.linkedin.com/in/saagarr/">
  <img align="left" alt="Linkdein" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
</a>
<a href="https://github.com/Sagar1555">
  <img align="left" alt="Github" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/github.svg" />
</a>
<a href="https://www.instagram.com/______.sagar.____/">
  <img align="left" alt="Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />
</a>
<a href="https://www.facebook.com/saagarjr7/">
  <img align="left" alt="Facebook" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/facebook.svg" />
</a>

<br/>
<br/>


# Examples of Work 

<h1> Web Development: </h1>

Personal Site

![Personal Site](https://github.com/Sagar1555/Sagar1555/blob/main/personal%20Website.PNG)

ANB Network

![ANB/Bagmati Network](https://github.com/Sagar1555/Sagar1555/blob/main/ANB.png)

### React Js Project 


Coming Soon 🔜 

___



<div align="center">

### Show some ❤️ by starring some of the repositories!

</div>

___

# Lets Build Something 

![Dino](https://raw.githubusercontent.com/wangningkai/wangningkai/master/assets/dino.gif)

